import config from '../config';
import EventEmitter from 'eventemitter3';

const EVENTS = {
    APP_READY: 'app_ready',
};

/**
 * App entry point.
 * All configurations are described in src/config.js
 */
export default class Application extends EventEmitter {
    constructor() {
        super();

        this.config = config;
        this.data = {};

        this.init();
    }

    static get events() {
        return EVENTS;
    }

    /**
     * Initializes the app.
     * Called when the DOM has loaded. You can initiate your custom classes here
     * and manipulate the DOM tree. Task data should be assigned to Application.data.
     * The APP_READY event should be emitted at the end of this method.
     */
    async init() {
        // Initiate classes and wait for async operations here.

        const response = await fetch('https://swapi.booost.bg/api/planets/');
        const data = await response.json();

        console.log(data);

        const planetsCount = data.count;
        const allPlanets = [];

        for (let i = 1; i <= 6; i++) {

            const nextPlanetsUrl = `https://swapi.booost.bg/api/planets/?page=${i}`;
            const eachPagePlanets = await fetch(nextPlanetsUrl);
            const fetchedData = await eachPagePlanets.json();

            const currentPlanets = fetchedData.results;

            currentPlanets.map(p => allPlanets.push(p));


        }

        console.log(allPlanets);

        this.data.count = planetsCount;
        this.data.planets = allPlanets;

        this.emit(Application.events.APP_READY);
    }
}